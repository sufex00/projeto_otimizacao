#include<stdio.h>
#include<math.h>
#include<time.h>
#include<string.h>

long int f1()
{
	clock_t start_t, end_t;
	double v[3] ;
	memset(v, 0, sizeof v);
	start_t = clock();
	for(int i = 0 ; i < 1000 ; i++)
	{
		v[i%2] += sin((int)(i/16))/1000;
	}

	end_t = clock();
	//printf("Tempo total = %ld", end_t - start_t);
	return end_t-start_t;
}

long int f2()
{
	clock_t start_t, end_t;
	double v[3] ;
	memset(v, 0, sizeof v);
	int x;
	start_t = clock();
	for(int i = 0 ; i < 1000 ; i++)
	{
		x = i>>4;
		v[i&2] += x*(1+x*(x*x*x/120-x/6));
	}
	v[0] = v[1]/1000;
	v[1] = v[2]/1000;
	end_t = clock();
	//printf("Tempo total = %ld", end_t - start_t);
	return end_t-start_t;
}
void main_t(int n)
{
	long long f1_sum = 0;
	long long f2_sum = 0;
	for(int i = 0 ; i < n ; i++)
		f1_sum += f1();
	double aux = f1_sum;
	for(int i = 0 ; i < n ; i++)
		f2_sum += f2();
	double aux2 = f2_sum;
	//printf("Estatistica:\n ");
	//printf("Porcen de ganho : %.2lf%%\n", aux2/aux * 100);
	printf("%d, %.2lf\n", n, (1 - aux2/aux)*100);
}

main()
{
	for(int i=0; i < 10000 ; i++)
		main_t(10);
}


