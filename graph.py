#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 10:44:04 2017

@author: pedrobarros
"""

import matplotlib.pyplot as plt
import pandas
import math

w =pandas.Series.from_csv('/home/pedrobarros/Downloads/projeto_otimizacao/stats.out')
w = w[:1000]
plt.plot(w, 'r')
plt.ylabel('% de ganho')
plt.xlabel('num de amostras usadas')
plt.show()

x = []
y = []

for i in range(0,1000):
    d = int(i/16);
    d = d * 180 / 3.141516
    x.append(math.sin(d))
    y.append(d*(1+d*(d*d*d/120-d/6)))
    

plt.plot(x,y, 'b')
plt.ylabel('Diferenca entre senos')
plt.xlabel('tempo')
plt.show()